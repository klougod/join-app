export const companyName = "JOIN";

export const instagramUrl = "https://www.instagram.com/";

export const twitterUrl = "https://twitter.com/";

export const facebookUrl = "https://facebook.com/";

export const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const nameRegex = /^[\sa-zA-Z0-9\u00C0-\u024F\u1E00-\u1EFF]{3,60}$/;

export const cpfRegex = /^[0-9]{3}[.]{1}[0-9]{3}[.]{1}[0-9]{3}[-]{1}[0-9]{2}$/;

export const cellphoneRegex = /^[\s0-9()-]{11,16}$/;

export const plateRegex = /^[a-zA-Z0-9\u00C0-\u024F\u1E00-\u1EFF]{0,10}$/;

export const referCodeRegex = /^[a-zA-Z0-9]{8}/;

export const yearRegex = /^(19|20)\d{2}$/;

export const baseApiUrl = 'https://joinappapi.azurewebsites.net';