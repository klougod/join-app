export default class Login {
  constructor({ email, password }) {
    this.email = email;
    this.password = password;
  }

  get json() {
    return JSON.stringify({
      Email: this.email,
      Password: this.password
    });
  }
}
