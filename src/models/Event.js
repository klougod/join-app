import { faBasketballBall, faBiking, faWalking, faDumbbell, faFootballBall,
  faSkating, faSpa, faStreetView, faFutbol, faTableTennis, faRunning,
  faVolleyballBall } from "@fortawesome/free-solid-svg-icons";

  
export default class Event {
  constructor({ eventId, typeId, longitude, latitude, date, name, description, address, isOwner }) {
    this.eventId = eventId;
    this.typeId = typeId;
    this.longitude = longitude;
    this.latitude = latitude;
    this.date = date;
    this.name = name;
    this.description = description;
    this.address = address;
    this.isOwner = isOwner ? isOwner : false;
  }

  get json() {
    return JSON.stringify({
      EventId: this.eventId,
      TypeId: this.typeId,
      Longitude: this.longitude,
      Latitude: this.latitude,
      Date: this.date,
      Name: this.name,
      Description: this.description,
      Address: this.address
    });
  }

  getIcon() {
    const possibleIcons = {
      1: faFutbol,
      2: faFutbol,
      3: faTableTennis,
      4: faRunning,
      5: faVolleyballBall,
      6: faBasketballBall,
      7: faBiking,
      9: faTableTennis,
      10: faWalking,
      11: faDumbbell,
      13: faFootballBall,
      14: faVolleyballBall,
      15: faSpa,
      16: faSkating,
      17: faFootballBall,
      23: faSpa
    }
    const icon = possibleIcons[this.typeId];
    return icon ? icon : faStreetView;
  }

  static parse(data = {
    EventId: 0,
    TypeId: 0,
    Longitude: -23,
    Latitude: -46,
    Date: '',
    Name: '',
    Description: '',
    Address: '',
    isOwner: false
  }) {
    return new Event({
      eventId: data.EventId ? data.eventId : data.Id,
      typeId: data.TypeId,
      longitude: data.Longitude,
      latitude: data.Latitude,
      date: data.Date,
      name: data.Name,
      description: data.Description,
      address: data.Address,
      isOwner: data.isOwner
    });
  }
}
