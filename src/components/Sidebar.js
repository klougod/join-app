import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { scaleDown as Menu } from 'react-burger-menu';
import { faBars } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { home, addEvent, enterEvent, userEvents, eventsToHappen } from '../routers/paths';
import { Row, Col } from 'react-bootstrap';
import { startLogout } from '../actions/auth';


export const Sidebar = ({ user, pageWrapId, outerContainerId, logout, noMenu }) => {

  return ( !noMenu &&
    <Menu pageWrapId={pageWrapId} outerContainerId={outerContainerId}
      customBurgerIcon={<FontAwesomeIcon className='custom-size' icon={faBars} />} >
      <Row noGutters>
        <Col>
          <img src="/images/logo.png" className="rounded-circle" height="100px" width="100px" alt="profile" />
        </Col>
        <Col className="align-middle vcenter">
          <h6 className='capitalize'>
            { `${user.firstName} ${user.lastName}`}
            <br />
            <small className="text-muted">ativo</small>
          </h6>
        </Col>
      </Row>
      <hr />
      <Link to={ home }>
        Home
      </Link>
      <Link to={ addEvent }>
        Adicionar evento
      </Link>
      <Link to={ enterEvent }>
        Participar de evento
      </Link>
      <Link to={ userEvents }>
        Meus eventos
      </Link>
      <Link to={ eventsToHappen }>
        Próximos eventos
      </Link>
      <Link to="/use-terms">
        Termos de uso
      </Link>
      <button className="btn btn-link no-padding" onClick={() => logout()}>
        Deslogar
      </button>
    </Menu>
  );
}

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(startLogout())
});

const mapStateToProps = (state) => ({
  user: state.user
});

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
