import { postAddEvent, getUserEvents, postFilteredEvents, postParticipateEvent,
  deleteEvent as sendDeleteEvent } from '../webservices/event';
import Event from '../models/Event';
import { setSessionItem } from '../helpers';


export const setEvent = ({ event, isEnded, error }) => ({
  type: 'SET_EVENT',
  event,
  isEnded,
  error
});

export const updateEvents = updates => ({
  type: 'UPDATE_EVENTS',
  updates
});

export const setEvents = events => ({
  type: 'SET_EVENTS',
  events
});

export const deleteEvent = updates => ({
  type: 'DELETE_EVENT',
  updates
});

export const updateEvent = updates => ({
  type: 'UPDATE_EVENT',
  updates
});

export const startSetEvents = () => {
  return (dispatch, getState) => {
    const token = getState().auth.token;
    const events = { events: {}, isEnded: false, error: false }
    if(token) {
      return getUserEvents(token).then(({ data }) => {
        if (data !== 'Unauthorized') {
          data.forEach(event => {
            event.isOwner = true;
            const parsedEvent = Event.parse(event);
            events.events[parsedEvent.eventId] = parsedEvent;
          });
        }
        else {
          events.error = true;
        }
      }).catch(err => {
        events.error = true;
        console.log(err);
      }).finally(() => {
        setSessionItem('events', events);
        dispatch(setEvents(events));
      });
    }
  }
};

export const getNearEvents = (coords) => {
  return (dispatch, getState) => {
    const token = getState().auth.token;
    const events = getState().events;
    return token && postFilteredEvents({ Location: { Latitude: coords[0], Longitude: coords[1] }, Radius: 20000 }, token)
      .then(({ data }) => {
        if (data !== 'Unauthorized') {
          data.forEach(event => {
            const parsedEvent = Event.parse(event);
            events.events[parsedEvent.eventId] = parsedEvent;
          });
        }
        else {
          events.error = true;
        }
      }).catch(err => {
        events.error = true;
        console.log(err);
      }).finally(() => {
        setSessionItem('events', events);
        dispatch(setEvents(events));
      });
  }
};

export const startAddEvent = eventData => {
  return (dispatch, getState) => {
    let event = new Event({});
    const isEnded = true;
    let error = false;
    const token = getState().auth.token;
    return postAddEvent(new Event(eventData).json, token).then(({ data }) => {
      if (data !== 'Unauthorized') {
        event = Event.parse(data);
      }
      else {
        error = true;
      }
    }).catch(err => {
      error = true;
      console.log(err);
    }).finally(() => {
      dispatch(setEvent({ event, isEnded, error }));
    });
  };
};

export const startDeleteEvent = eventId => {
  return (dispatch, getState) => {
    const isEnded = true;
    let error = false;
    const token = getState().auth.token;
    return sendDeleteEvent(eventId, token).then(({ data }) => {
      console.log(data)
      if (data === 'Unauthorized') {
        error = true;
      }
    }).catch(err => {
      error = true;
      console.log(err);
    }).finally(() => {
      dispatch(deleteEvent({ eventId, error, isEnded }));
    });
  };
};

export const startUpdateEvent = eventData => {
  return (dispatch, getState) => {
    let event = new Event(eventData);
    const isEnded = true;
    let error = false;
    console.log(event)
    const token = getState().auth.token;
    return postAddEvent(event.json, token).then(({ data }) => {
      console.log(data)
      if (data !== 'Unauthorized') {
        //event = Event.parse(data);
      }
      else {
        error = true;
      }
    }).catch(err => {
      error = true;
      console.log(err);
    }).finally(() => {
      dispatch(updateEvent({ event, isEnded, error }));
    });
  };
};

export const startParticipateEvent = id => {
  return (dispatch, getState) => {
    const token = getState().auth.token;
    let error = false;
    return postParticipateEvent(id, token).then(({ data }) => {
      if (data === 'Unauthorized') {
        error = true;
      }
    }).catch(err => {
      error = true;
      console.log(err);
    }).finally(() => {
      dispatch(updateEvents({ isEnded: true, error }));
    });
  };
};

export const updateEventsStatus = status => {
  return (dispatch) => {
    dispatch(updateEvents({ isEnded: status }))
  }
};
