import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Map, Marker, Popup, TileLayer } from 'react-leaflet';
import { renderToStaticMarkup } from 'react-dom/server';
import { divIcon } from 'leaflet';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMapMarkerAlt, faHome, faBuilding, faFilter } from "@fortawesome/free-solid-svg-icons";
import BasePage from './BasePage';
import Control from 'react-leaflet-control';
import moment from 'moment';
import { Dropdown, Row, Col } from 'react-bootstrap';
import { getNearEvents, startSetEvents } from '../actions/events';
import EventList from '../models/EventList';
import { MarkersList } from '../components/popups';
import { getLocalStorageItem, setLocalStorageItem } from '../helpers';


const isNight = moment().isAfter(moment({ hour:18, minute: 0 })) || moment().isBefore(moment({ hour:6, minute: 0 }));

const HomePage = ({ user, events, getNearEvents, getOwnEvents }) => {

  const [position, setPositon] = useState([-23.5486, -46.6392]);
  const [actualPosition, setActualPositon] = useState([-23.5486, -46.6392]);
  const [zoom, setZoom] = useState(15);
  const [themeMode, setThemeMode] = useState(isNight ? 'dark' : 'light');
  const [iconColor, setIconColor] = useState(themeMode === 'dark' ? '#dddddd' : 'black');
  const markers = new EventList(events).makeMarkers(iconColor);

  if (user) {
    if (user.home[0] === user.work[0] && user.home[1] === user.work[1]) {
      markers.push({
        key: 'homeMarker', position: [user.home[0], user.home[1]], name: 'Home', icon: faHome,
        color: iconColor, description: 'Home sweet home', type: 'single'
      });
    }
    else {
      markers.push({
        key: 'homeMarker', position: [user.home[0], user.home[1]], name: 'Home', icon: faHome, 
        color: iconColor, description: 'Home sweet home', type: 'single'
      });
      markers.push({
        key: 'workMarker', position: [user.work[0], user.work[1]], name: 'Work', icon: faBuilding,
        color: iconColor, description: 'Work sweet ... well, just work', type: 'single'
      });
    }
  }

  useEffect(() => {
    getOwnEvents();
    const pos = getLocalStorageItem('pos');
    if (pos) {
      setActualPositon(pos);
      getNearEvents(pos);
    }
    else {
      askLocationPermission();
    }
    // eslint-disable-next-line
  }, [])

  const changeThemeColor = () => {
    setIconColor(themeMode === 'dark' ? 'black' : '#dddddd');
    setThemeMode(themeMode === 'dark' ? 'light': 'dark');
  }

  const askLocationPermission = () => {
    if ("geolocation" in navigator) {
      // check if geolocation is supported/enabled on current browser
      navigator.geolocation.getCurrentPosition(position => {
        const pos = [position.coords.latitude, position.coords.longitude];
          setActualPositon(pos);
          setPositon(pos);
          setLocalStorageItem('pos', pos);
        }
      );
    }
  }

  const handleMarkerChange = e => {
    const {lat, lng} = e.target.getLatLng();
    setPositon([lat, lng]);
    setActualPositon([lat, lng]);
  }

  const iconMarkup = renderToStaticMarkup(<FontAwesomeIcon size="2x" icon={ faMapMarkerAlt } color={iconColor} />);
  const customMarkerIcon = divIcon({ html: iconMarkup });

  return (
    <BasePage headerClass="header__bg" title="Home">
      <Map center={ position } style={{ height:"100%" }} zoom={ zoom } maxZoom={ 18 } onZoomEnd={e => setZoom(e.target.getZoom())}>
        <TileLayer url={`https://cartodb-basemaps-{s}.global.ssl.fastly.net/${themeMode}_all/{z}/{x}/{y}.png`} />
        <Marker icon={ customMarkerIcon } position={ actualPosition } draggable onMoveEnd={ handleMarkerChange }>
          <Popup>
            <Row noGutters>
              <Col sm={12} className='bg-primary popup__col popup__col_main'>
                Carregar Eventos Próximos
              </Col>
              <Col sm={12} className='popup__col popup__col_button'>
                <button className='btn btn-outline-primary btn-sm' onClick={() => getNearEvents(actualPosition)}>sim</button>
              </Col>
            </Row>
          </Popup>
        </Marker>
        <MarkersList markers={markers} />
        <Control position="topright">
          <Dropdown drop='left' name='filtermap'>
            <Dropdown.Toggle id="dropdown-filtermap">
              <FontAwesomeIcon size="3x" icon={ faFilter } color={iconColor} />
            </Dropdown.Toggle>
            <Dropdown.Menu>
              <Dropdown.Item as='button' onClick={e => {e.preventDefault();changeThemeColor();}}>
                {themeMode === 'dark' ? 'Light' : 'Dark'} theme
              </Dropdown.Item>
              <Dropdown.Item as='button' onClick={e => {e.preventDefault();askLocationPermission();}}>
                Center
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </Control>
        <Control position="bottomright">
          <button className='btn btn-lg btn__icon btn__icon_center' onClick={() => setPositon(user.work)}>
            <FontAwesomeIcon  size="2x" icon={ faBuilding } />
          </button>
          <button className='btn btn__icon btn__icon_right' onClick={() => setPositon(user.home)}>
            <FontAwesomeIcon  size="2x" icon={ faHome }  />
          </button>
        </Control>
      </Map>
    </BasePage>
  );
}

const mapDispatchToProps = (dispatch) => ({
  getNearEvents: pos => dispatch(getNearEvents(pos)),
  getOwnEvents: () => dispatch(startSetEvents())
});

const mapStateToProps = (state) => ({
  user: state.user,
  events: state.events.events
});

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
