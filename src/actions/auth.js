import { postRegister, postLogin } from '../webservices/login';
import Login from '../models/Login';
import User from '../models/User';
import { setUser } from './user';
import { getLocalStorageItem, setLocalStorageItem, removeLocalStorageItem } from '../helpers';


export const setAuth = login => ({
  type: 'SET_AUTH',
  login
});

export const startSetAuth = () => {
  return (dispatch) => {
    const auth = getLocalStorageItem('auth');
    dispatch(setAuth(auth));
  };
}

export const startLogin = (loginData) => {
  return (dispatch) => {
    const login = new Login(loginData);
    const auth = {
      token: undefined,
      isEnded: true,
      error: false
    }
    return postLogin(login.json).then(({ data }) => {
      if (data === "Unauthorized") {
        auth.error = true;
      }
      else {
        const user = User.parse(data.User);
        auth.token = data.SessionToken ? data.SessionToken : null;
        auth.error = false;
        setLocalStorageItem('user', user);
        dispatch(setUser(user));
      }
    }).catch(err => {
      console.log(err);
      auth.error = true;
    }).finally(() => {
      setLocalStorageItem('auth', { ...auth, isEnded: false });
      dispatch(setAuth(auth));
    });
  };
};

export const startRegister = (userData) => {
  return (dispatch) => {
    const user = new User(userData);
    const auth = {
      token: undefined,
      isEnded: true,
      error: false
    }
    postRegister(user.json).then(({ data }) => {
      if(data.Success) {
        auth.error = false;
        auth.token = data.SessionToken;
        const user = User.parse(data.User);
        setLocalStorageItem('user', user);
        dispatch(setUser(user));
      }
      else {
        auth.error = true;
      }
    }).catch(err => {
      console.log(err);
      auth.error = true;
    }).finally(() => {
      setLocalStorageItem('auth', { ...auth, isEnded: false });
      dispatch(setAuth(auth));
    });
  };
};

export const updateAuthStatus = status => {
  return (dispatch) => {
    dispatch(setAuth({ isEnded: status }))
  }
};

export const logout = () => ({
  type: 'LOGOUT'
});

export const startLogout = () => {
  return (dispatch) => {
    removeLocalStorageItem('auth');
    removeLocalStorageItem('user');
    dispatch(logout());
  };
}