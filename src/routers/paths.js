// Routes paths may be used and changed only here

export const home = "/";
export const login = "/login";
export const register = "/register";
export const addEvent = '/events/add';
export const events = '/events';
export const enterEvent = '/events/enter';
export const editEvent = '/event/:id';
export const userEvents = '/user/events';
export const eventsToHappen = '/events/next';