import { authGet, authPost, authDelete } from './base';


export const postAddEvent = (data, token) => {
  return authPost({ url: '/Events', token, data });
}

export const getEventTypes = token => {
  return authGet({ url: '/Events/Types', token });
}

export const getUserEvents = token => {
  return authGet({ url: '/Events/Own', token });
}

export const postFilteredEvents = (data, token) => {
  return authPost({ url: '/Events/Filter', token, data });
}

export const postParticipateEvent = (id, token) => {
  return authPost({ url: `/Events/Participate(${id})`, token, data: null });
}

export const getParticipantsEvent = (id, token) => {
  return authGet({ url: `/Events/Participants(${id})`, token });
}

export const getEventsToHappen = (token) => {
  return authGet({ url: '/Events/ToHappen', token });
}

export const deleteEvent = (id, token) => {
  return authDelete({ url: `/Events(${id})`, token });
}