import { getLocalStorageItem } from '../helpers';


export const setUser = user => ({
  type: 'SET_USER',
  user
});

export const startSetUser = () => {
  return (dispatch) => {
    const user = getLocalStorageItem('user');
    dispatch(setUser(user));
  };
};
