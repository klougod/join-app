export default class User {
  constructor({firstName, lastName, home, work, email, password, status, themeId }) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.home = home ? home : null;
    this.work = work ? work : null;
    this.password = password;
    this.status = status;
    this.themeId = themeId;
  }

  get json() {
    return JSON.stringify({
      FirstName: this.firstName,
      LastName: this.lastName,
      Home: {
        Latitude: this.home[0],
        Longitude: this.home[1],
      },
      Work: {
        Latitude: this.work[0],
        Longitude: this.work[1],
      },
      Email: this.email,
      Password: this.password,
      Status: this.status,
      ThemeId: this.themeId
    });
  }

  static parse(data = { 
    FirstName: null, 
    LastName: null, 
    Home: {
      Latitude: null,
      Longitude: null
    },
    Work: {
      Latitude: null,
      Longitude: null
    }, 
    Email: null, 
    ThemeId: null,
    Status: null
  }) {
    return new User({
      firstName: data.FirstName,
      lastName: data.LastName,
      home: [data.Home.Latitude, data.Home.Longitude],
      work: [data.Work.Latitude, data.Work.Longitude],
      email: data.Email,
      password: null,
      status: data.Status,
      themeId: data.ThemeId
    });
  }
}