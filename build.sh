#!/bin/bash

# shellcheck source=VERSIONS
set -o allexport; source VERSIONS; set +o allexport || exit 1

JOIN_VERSION=v0.3.2

IS_VERSION_CHANGED=false

if [ "$JOIN_VERSION" != "$JOIN_CURRENT" ]; then
  # it has a no-cache because if not it doesnt change some things in build, 
  # even when the src has changed..., I wish I knew why!
  docker build . -f ./Dockerfile -t join_app --no-cache || exit 1
  docker tag join_app ignaciopinedo/join_app:$JOIN_VERSION || exit 1
  docker push ignaciopinedo/join_app:$JOIN_VERSION
  sed -i "s/JOIN_CURRENT=.*/JOIN_CURRENT=$JOIN_VERSION/" VERSIONS || exit 1
  IS_VERSION_CHANGED=true
fi

if [ "$IS_VERSION_CHANGED" = true ] ; then
  git config user.email "$CIRCLECI_EMAIL"
  git config user.name "Gitlab Bot"
  git add VERSIONS || exit 1
  git commit -m "changed current prod versions [skip ci]" || exit 1
  git push "https://klougod:${CI_TOKEN}@${CI_REPOSITORY_URL#*@}" "HEAD:refs/heads/master" || exit 1
fi