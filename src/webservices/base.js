import Axios from 'axios';
import { baseApiUrl } from '../constants';


export const post = ({ url, data }) => {
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  };
  return Axios.post(`${baseApiUrl}${url}`, data, config);
}

export const get = ({ url }) => {
  return Axios.get(`${baseApiUrl}${url}`);
}

export const authPost = ({ url, data, token }) => {
  const config = {
    headers: {
      'uk': token,
      'Content-Type': 'application/json'
    }
  };
  return Axios.post(`${baseApiUrl}${url}`, data, config);
}

export const authGet = ({ url, token }) => {
  const config = {
    headers: {'uk': token}
  };
  return Axios.get(`${baseApiUrl}${url}`, config);
}

export const authDelete = ({ url, token }) => {
  const config = {
    headers: {
      'uk': token,
      'Content-Type': 'application/json'
    }
  };
  return Axios.delete(`${baseApiUrl}${url}`, config);
}