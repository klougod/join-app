import React from 'react';
import { connect } from 'react-redux';
import BasePage from './BasePage';
import EditEventBox from '../components/EditEventBox';


const UserEventsPage = ({ events }) => {
  const eventsArray = []
  //eslint-disable-next-line
  for (let key in events) {
    if (events.hasOwnProperty(key)) {
      const event = events[key];
      eventsArray.push(event);
    }
  }
  return (
    <BasePage headerClass="header__bg" title="Meus Eventos">
      {
        eventsArray.map((event, i) => {
          return <EditEventBox event={event} key={i} />
        })
      }
    </BasePage>
  )
};

const mapStateToProps = (state) => ({
  events: state.events.events
});

export default connect(mapStateToProps)(UserEventsPage);