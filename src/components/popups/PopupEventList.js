import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';
import { divIcon } from 'leaflet';
import { Marker, Popup } from 'react-leaflet';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Row, Col } from 'react-bootstrap';
import { history } from  '../../routers/MainRouter';


const PopupEventList = ({ events, icon, color }) => {
  const iconMarkup = renderToStaticMarkup(<FontAwesomeIcon size="2x" icon={icon} color={color ? color : 'black'} />);
  const customMarkerIcon = divIcon({ html: iconMarkup });
  return(
    <Marker position={events ? events[0].position : [-23, -46]} icon={customMarkerIcon}>
      <Popup>
        <Row noGutters>
          <Col sm={12} className='bg-warning popup__col popup__col_title'>
            Eventos no local
          </Col>
          {
            events && events.map(event => (
              <Col key={event.id} sm={12} className='popup__col popup__col_list'>
                {event.name}
                <button className='btn btn-outline-primary btn-sm' onClick={() => history.push(`/Event/${event.id}`)}>Ver</button>
              </Col>
            ))
          }
        </Row>
      </Popup>
    </Marker>
  )
}

export default PopupEventList;