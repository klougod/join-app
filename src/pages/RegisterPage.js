import React from 'react';
import BasePage from './BasePage';
import RegisterForm from '../components//RegisterForm';


const RegisterPage = props => {

  return (
    <BasePage noMenu headerClass="header__bg" title="Registrar">
      <RegisterForm />
    </BasePage>
  );
}

export default RegisterPage;
