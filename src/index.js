import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { HelmetProvider } from 'react-helmet-async';
import configureStore from './store/configureStore';
import { startSetAuth } from './actions/auth';
import { startSetUser } from './actions/user';
import { startSetEvents } from './actions/events';
import MainRouter from './routers/MainRouter';
import './styles/styles.scss';
import * as serviceWorker from './serviceWorker';

const store = configureStore();

store.dispatch(startSetAuth());
store.dispatch(startSetUser());
store.dispatch(startSetEvents());

const jsx = (
  <HelmetProvider>
    <Provider store={store}>
      <MainRouter />
    </Provider>
  </HelmetProvider>
);

ReactDOM.render(jsx, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();