import { faStreetView } from "@fortawesome/free-solid-svg-icons";
import Event from './Event';


export default class EventList {
  constructor(events) {
    // need to be a deep copy so it doesnt fuck saved state in redux!
    this.eventsHash = JSON.parse(JSON.stringify(events));
    this._groupEqualPositions();
  }

  _groupEqualPositions() {
    const duplicatedEvents = {};
    //eslint-disable-next-line
    for (let i in this.eventsHash) {
      if (this.eventsHash.hasOwnProperty(i)) {
        const actualEvent = this.eventsHash[i];
        //eslint-disable-next-line
        for (let j in this.eventsHash) {
          if (this.eventsHash.hasOwnProperty(j)) {
            const compEvent = this.eventsHash[j];
            if (compEvent.eventId !== actualEvent.eventId && compEvent.latitude === actualEvent.latitude &&
                compEvent.longitude === actualEvent.longitude) {
              duplicatedEvents[i] = [
                {id: actualEvent.eventId, name: actualEvent.name, position: [actualEvent.latitude, actualEvent.longitude]},
                {id: compEvent.eventId, name: compEvent.name, position: [compEvent.latitude, compEvent.longitude]},
              ];
              delete this.eventsHash[i];
              delete this.eventsHash[j];
            }
          }
        }
      }
    }
    this.duplicatedEventsHash = duplicatedEvents;
  }

  makeMarkers(iconColor) {
    const markers = [];
    const events = this.eventsHash;
    const duplicatedEvents = this.duplicatedEventsHash;
    //eslint-disable-next-line
    for (let key in events) {
      if (events.hasOwnProperty(key)) {
        const event = new Event(events[key]);
        markers.push({
          type: 'single',
          key,
          position: [event.latitude, event.longitude],
          name: event.name,
          description: event.description,
          icon: event.getIcon(),
          date: event.date,
          eventId: event.eventId,
          color: iconColor
        });
      }
    }
    //eslint-disable-next-line
    for (let key in duplicatedEvents) {
      if (duplicatedEvents.hasOwnProperty(key)) {
        const duplicatedEvent = duplicatedEvents[key];
        markers.push({
          type: 'list',
          key,
          events: duplicatedEvent,
          icon: faStreetView,
          color: iconColor
        });
      }
    }
    return markers;
  }

  get events() {
    return this.eventsHash;
  }

  get duplicatedEvents() {
    return this.duplicatedEventsHash;
  }
}