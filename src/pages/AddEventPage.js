import React from 'react';
import BasePage from './BasePage';
import AddEventForm from '../components/AddEventForm';


const AddEventPage = props => {

  return (
    <BasePage headerClass="header__bg" title="Adicionar Evento">
      <AddEventForm />
    </BasePage>
  );
}

export default AddEventPage;
