// Event Reducer

export default (state = {events: {}, isEnded: false, error: false}, action) => {
  switch (action.type) {
    case 'SET_EVENT':
      const events = state.events;
      events[action.event.eventId] = action.event;
      return {
        events: events,
        isEnded: action.isEnded,
        error: action.error
      };
    case 'SET_EVENTS':
      let newEvents = {};
      const actionEvents = action.events.events;
      const stateEvents = state.events;
      // eslint-disable-next-line
      for (let key in actionEvents) {
        if (actionEvents.hasOwnProperty(key)) {
          const event = actionEvents[key];
          newEvents[event.eventId] = event;
        }
      }
      if (stateEvents) newEvents = {...state.events, ...newEvents};
      return {
        isEnded: action.events.isEnded,
        error: action.events.error,
        events: newEvents
      };
    case 'UPDATE_EVENTS':
      return {
        ...state,
        ...action.updates
      };
    case 'UPDATE_EVENT':
      const { event } = action.updates;
      state.events[event.eventId] = event;
      return {
        ...state,
        isEnded: action.updates.isEnded,
        error: action.updates.error
      }
    case 'DELETE_EVENT':
      const { eventId } = action.updates;
      delete state.events[eventId];
      return {
        ...state,
        isEnded: action.updates.isEnded,
        error: action.updates.error
      }
    default:
      return state;
  }
};