/* jshint shadow:true */
// a module to geocode mapbox in js
export default class MapBox {

  constructor(params = {}){
    const {
      proximity="-46.632998,-23.551427", 
      language="pt-BR", 
      autocomplete="true",
      types="address,poi",
      country="BR",
      key
    } = params;
    this.key = key;
    this.baseUrl = "https://api.mapbox.com/geocoding/v5/mapbox.places/";
    this.address = "";
    this.proximity = proximity;
    this.language = language
    this.autocomplete = autocomplete;
    this.types = types;
    this.country=country;
  }

  async geocode(address) {
    this.address = address;
    const url = this._buildUrl();
    const result = await this._geocodeAddress(url);
    const items = [];
    if(!result.error){
      // eslint-disable-next-line
      for(let i of result.data){
        items.push({id: i.id, address: i.place_name, lat: i.center[1], lng: i.center[0], neighborhood: i.context[0].text})
      }
    }
    return items;
  }

  _buildUrl() {
    return `${this.baseUrl}${this.address}.json?access_token=${this.key}&proximity=${this.proximity}`+
            `&types=${this.types}&coutry=${this.country}&autocomplete=${this.autocomplete}&bbox=-73.916016,-33.211116,-34.365234,5.178482`;
  }

  async _geocodeAddress(url, errorcb=null) {
    try {
      const response = await fetch(url);
      if (response.status !== 200) {
        errorcb ? errorcb(`Received status ${response.status}`) :
          console.log(`Error in request. Status Code: ${response.status}`);
        return {'error': true};
      }
      const results = await response.json();
      if(results.features){
        return {'error': false, 'data': results.features};
      }
      return {'error': true};
    }
    catch (err) {
      errorcb ? errorcb(err) : console.log(`Fetch Error : ${err}`);
      return {'error': true};
    }
  }
}

export const randomKey = () => {
  const keys = [
    "pk.eyJ1Ijoiam9hby1jbGViZXIiLCJhIjoiY2sweTluajd6MGNlcTNtbXZ3Nno0YndnYyJ9.IBZXUShpKAIqWhzFQxBeBg",
    "pk.eyJ1IjoiYWUtb2xpdmVpcmEiLCJhIjoiY2sweTl5aDU2MDF3djNsbzBoYWtlZG5ibSJ9.XkpVfAlelkLIVhzm1CwOpQ",
    "pk.eyJ1Ijoiam9hby1hbmFuaWFzIiwiYSI6ImNrMHlhMW12ZTBjaGMzYm12dTR0Z2FvaGQifQ.-3tJOVw2yapEH6Qtc8HCuw",
    "pk.eyJ1Ijoicm9iZXJ0LWNhcmxvcyIsImEiOiJjazB5YTJ6ZjkwMmk2M2RvNmtkMmNkaHk0In0._42RQeMdVoPYOgfETF2sQg"
  ];

  return keys[Math.floor(Math.random() * keys.length)];
}
