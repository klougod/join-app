import React, { useState, useEffect, useRef } from 'react';
import { connect } from 'react-redux';
import { history } from '../routers/MainRouter';
import { login, home } from '../routers/paths';
import { startRegister, updateAuthStatus } from '../actions/auth';
import PageContent from './base/PageContent';
import Autocomplete from './autocomplete';
import { emailRegex } from '../constants';
import Swal from 'sweetalert2';


const LoginForm = ({ startRegister, auth, updateAuthStatus }) => {

  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if(auth.isEnded){
      setIsLoading(false);
      updateAuthStatus(false);
      if(!auth.error){
        Swal.fire('Usuário cadastrado', 'obrigado', 'success').then(() =>{
          history.push(home);
        });
      }
      else {
        Swal.fire('Erro ao registrar usuario', 'tente novamente', 'warning');
      }
    }
  }, [auth, isLoading, updateAuthStatus])

  const [firstName, setFirstName] = useState('');
  const [firstNameError, setFirstNameError] = useState('');
  const [lastName, setLastName] = useState('');
  const [lastNameError, setLastNameError] = useState('');
  const [homeAddress, setHomeAddress] = useState('');
  const [homeCoords, setHomeCoords] = useState([0, 0]);
  const [workAddress, setWorkAddress] = useState('');
  const [workCoords, setWorkCoords] = useState([0, 0]);
  const [email, setEmail] = useState('');
  const [emailError, setEmailError] = useState('');
  const [password, setPassword] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [confirmPasswordError, setConfirmPasswordError] = useState('');

  const isInputValid = () => {
    if(firstName.length < 3) {
      setFirstNameError('o campo acima deve ter pelo menos 3 caracteres');
      return false;
    }
    else {
      setFirstNameError('');
    }
    if(lastName.length < 3) {
      setLastNameError('o campo acima deve ter pelo menos 4 caracteres');
      return false;
    }
    else {
      setLastNameError('');
    }
    if(!emailRegex.test(email)) {
      setEmailError('insira um email válido');
      return false;
    }
    else {
      setEmailError('');
    }
    if(password.length < 8){
      setPasswordError('o campo acima deve ter pelo menos 8 caracteres');
      return false;
    }
    else {
      setPasswordError('');
    }
    if(password !== confirmPassword){
      setConfirmPasswordError('as senhas devem ser iguais');
      return false;
    }
    else {
      setConfirmPasswordError('');
    }
    return true;
  }

  const handleSubmit = e => {
    e.preventDefault();
    setIsLoading(true);
    if(isInputValid()){
      const user = {
        firstName,
        lastName,
        home: homeCoords,
        work: workCoords,
        email,
        password,
        status: 'Ativo',
        themeId: 2
      }
      startRegister(user);
    } else {
      setIsLoading(false);
    }
  }

  const handleHomeAutocomplete = e => {
    setHomeAddress(e.address);
    if(e.lat && e.lng){
      setHomeCoords([e.lat, e.lng]);
    }
  }

  const handleWorkAutocomplete = e => {
    setWorkAddress(e.address);
    if(e.lat && e.lng){
      setWorkCoords([e.lat, e.lng]);
    }
  }

  return (
    <PageContent>
      <div className='row h-80'>
      </div>
      <form className='row h-80'>
        <div className="form-group col-12">
          <label htmlFor="firstName">Primeiro nome</label>
          <input type="text" className="form-control" placeholder="Enter first name" name="firstName"
            value={firstName} onChange={e => setFirstName(e.target.value)} />
          <small id="firstNameHelp" className="text-danger">
            {firstNameError}
          </small> 
        </div>
        <div className="form-group col-12">
          <label htmlFor="lastName">Sobrenome</label>
          <input type="text" className="form-control" placeholder="Enter last name"  name="lastName"
            value={lastName} onChange={e => setLastName(e.target.value)} />
          <small id="lastNameHelp" className="text-danger">
            {lastNameError}
          </small> 
        </div>
        <div className="form-group col-12">
          <label htmlFor="homeAddress">Endereço de casa</label>
          <Autocomplete inputRef={useRef(null)} value={homeAddress} name="homeAddress" placeholder="Enter home address"
            onSelectItem={handleHomeAutocomplete} />
        </div>
        <div className="form-group col-12">
          <label htmlFor="workAddress">Endereço do trabalho</label>
          <Autocomplete inputRef={useRef(null)} value={workAddress} name="workAddress" placeholder="Enter work address"
            onSelectItem={handleWorkAutocomplete} />
        </div>
        <div className="form-group col-12">
          <label htmlFor="email">Endereço de email</label>
          <input type="email" className="form-control" placeholder="Enter email" name="email"
            value={email} onChange={e => setEmail(e.target.value)} />
            <small id="emailHelp" className="text-danger">
              {emailError}
            </small> 
        </div>
        <div className="form-group col-12">
          <label htmlFor="password">Senha</label>
          <input type="password" className="form-control" placeholder="Password"  name="password"
            value={password} onChange={e => setPassword(e.target.value)} />
          <small id="passwordHelp" className="text-danger">
            {passwordError}
          </small> 
        </div>
        <div className="form-group col-12">
          <label htmlFor="exampleInputPassword1">Confirme a senha</label>
          <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Confirm password"
            value={confirmPassword} onChange={e => setConfirmPassword(e.target.value)}   name="confirmPassword" />
          <small id="confirmPasswordHelp" className="text-danger">
            {confirmPasswordError}
          </small> 
        </div>
        <div className='col-12'>
          <button type="submit" onClick={handleSubmit} className="btn btn-primary" disabled={isLoading}>
            {
              isLoading ?
              <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> :
              'Registrar'
            }
          </button>
        </div>
        <div className='col-12'>
          <br /> <br />
          <h5>Já possui uma conta?</h5>
          <button onClick={() => history.push(login)} className="btn btn-primary">Sign In</button>
        </div>
      </form>
    </PageContent>
  );
}

const mapDispatchToProps = (dispatch) => ({
  startRegister: user => dispatch(startRegister(user)),
  updateAuthStatus: status => dispatch(updateAuthStatus(status))
});

const mapStateToProps = (state) => ({
  auth: state.auth
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
