import React, { useState, useRef, useEffect } from 'react';
import AutocompleteItem from './AutocompleteItem';
import MapBox, { randomKey } from '../../webservices/mapbox';


export const Autocomplete = props => {

  const id = props.id ? props.id : 'id-not-provided';
  const name = props.name ? props.name : "";
  const placeholder = props.placeholder ? props.placeholder : "";
  const [address, setAddress] = useState("");
  const [suggestions, setSuggestions] = useState([]);
  const [prevSuggestions, setPrevSuggestions] = useState(null);
  const [isTyping, setIsTyping] = useState(false);
  const [currentFocus, setCurrentFocus] = useState(-1);
  const executeLimitRate = 300;
  const input = props.inputRef;
  const list = useRef(null);

  

  useEffect(() => {
    const handleClickout = e => {
      const isClickInside = e.target.classList.contains("autocomplete__item");
      if(!isClickInside){
         clearList();
      }
    }
    document.addEventListener('click', handleClickout);
    return () => {
      document.removeEventListener('click', handleClickout);
    };
  }, [suggestions]);

  useEffect(() => {
    const items = list.current.getElementsByTagName('div');
    if (!items) return;
    removeActive(items);
    if (currentFocus >= items.length) {
      setCurrentFocus(0); return;
    }
    if (currentFocus < 0) {
      setCurrentFocus(items.length - 1); return;
    }
    items[currentFocus].classList.add("autocomplete__item__active");
  }, [currentFocus]);

  const clearList = () => {
    setSuggestions([]);
  }

  const getSuggestions = () => {
    setIsTyping(false);
    const geocoder = new MapBox({ key: randomKey() });
    const value = input.current.value;
    if(value){
      geocoder.geocode(value).then((result) => {
        if(result.length <= 0 && prevSuggestions) {
          setSuggestions(prevSuggestions);
        }
        else {
          setSuggestions(result);
          setPrevSuggestions(result);
        }
      });
    }
  }

  const inputHandler = () => {
    if(!isTyping){
      setIsTyping(true);
      setTimeout(getSuggestions, executeLimitRate);
    }
  }

  const removeActive = items => {
    for (let i = 0; i < items.length; i++) {
      items[i].classList.remove("autocomplete__item__active");
    }
  }

  // This is not the best way with react, but is kind of a legacy from
  // the old autocomplete made with pure js, someday i'll wrap all items
  // and change this
  const keyDownHandler = e => {
    const items = list.current.getElementsByTagName('div');
    if(e.keyCode === 40){
      setCurrentFocus(prev => prev + 1);
    }
    else if(e.keyCode === 38){
      setCurrentFocus(prev => prev - 1);
    }
    else if(e.keyCode === 9){
      if (currentFocus > -1) {
        if (items.length > 0) items[currentFocus].click();
      }
    }
    else if(e.keyCode === 13){
      e.preventDefault();
      if (currentFocus > -1) {
        if (items.length > 0) items[currentFocus].click();
      }
    }
  }

  const setInputValue = address => {
    setAddress(address)
    clearList();
  }

  const onChangeAddr = e => {
    const address = e.target.value;
    setAddress(address);
    props.onSelectItem({address});
  }

  return (
    <div className="autocomplete__container">
        <input
          autoComplete="off"
          ref={input}
          onInput={inputHandler}
          onKeyDown={keyDownHandler}
          onChange={onChangeAddr}
          value={address}
          id={id}
          name={name}
          type="text"
          placeholder={placeholder}
          className="form-control"
        />
        <div ref={list} className="autocomplete__items">
        {
          suggestions.length !== 0 && (suggestions.map((suggestion) => {
            return <AutocompleteItem
                      key={suggestion.id}
                      {...suggestion}
                      onSelectItem={props.onSelectItem}
                      clearList={clearList}
                      setInputValue={setInputValue}
                    />;
          }))
        }
        </div>
    </div>
  );

}

export default Autocomplete;
