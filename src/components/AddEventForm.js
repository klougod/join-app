import React, { useState, useEffect, useRef } from 'react';
import { connect } from 'react-redux';
import Swal from 'sweetalert2';
import { history } from '../routers/MainRouter';
import { home } from '../routers/paths';
import { startAddEvent, updateEventsStatus } from '../actions/events';
import { getEventTypes } from '../webservices/event';
import PageContent from './base/PageContent';
import Autocomplete from './autocomplete';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import ptBr from 'date-fns/locale/pt-BR';


const AddEventForm = ({ startAddEvent, events, token, updateEventsStatus }) => {
  const [name, setName] = useState('');
  const [date, setDate] = useState(moment());
  const [description, setDescription] = useState('');
  const [eventAddress, setEventAddress] = useState('');
  const [eventTypes, setEventTypes] = useState([]);
  const [eventCoords, setEventCoords] = useState([0, 0]);
  const [selectedType, setSelectedType] = useState(0);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    getEventTypes(token).then(({ data }) => {
      setEventTypes(data);
    }).catch(err => {
      console.log(err);
    })
  // eslint-disable-next-line
  }, [])

  useEffect(() => {
    if(events.isEnded){
      setIsLoading(false);
      updateEventsStatus(false);
      if(!events.error){
        Swal.fire('Evento adicionado', 'obrigado', 'success').then(() =>{
          history.push(home);
        });
      }
      else {
        Swal.fire('Erro ao adicionar evento', 'tente novamente', 'warning');
      }
    }
  // eslint-disable-next-line
  }, [events])

  const handleAutocomplete = e => {
    setEventAddress(e.address);
    if(e.lat && e.lng){
      setEventCoords([e.lat, e.lng]);
    }
  }

  const handleSubmit = e => {
    e.preventDefault();
    setIsLoading(true);
    if(name && eventAddress){
      const event = {
        typeId: selectedType,
        latitude: eventCoords[0],
        longitude: eventCoords[1],
        date: date.format('MM/D/YYYY HH:mm'),
        name,
        description,
        address: eventAddress
      }
      startAddEvent(event);
    }
    else {
      Swal.fire('Erro ao adicionar evento', 'tente novamente', 'warning');
      setIsLoading(false);
    }
  }

  return (
    <PageContent>
      <form className='row justify-content-center'>
        <div className="form-group col-12">
          <label htmlFor="name">Nome do evento</label>
          <input type="text" className="form-control" placeholder="Enter event name" name="name"
            value={name} onChange={e => setName(e.target.value)} />
        </div>
        <div className="form-group col-12">
          <label htmlFor="description">Descrição do evento</label>
          <input type="text" className="form-control" placeholder="Enter event description" name="description"
            value={description} onChange={e => setDescription(e.target.value)} />
        </div>
        <div className="form-group col-12">
          <label htmlFor="eventAddress">Endereço do evento</label>
          <Autocomplete inputRef={useRef(null)} value={eventAddress} name="eventAddress" placeholder="Enter event address"
            onSelectItem={handleAutocomplete} />
        </div>
        <div className="form-group col-12">
          <label htmlFor="eventTime">Data e hora do evento</label>
          <DatePicker withPortal selected={date.toDate()} onChange={date => {setDate(moment(date));}} showTimeSelect showDisabledMonthNavigation
            timeFormat="HH:mm" timeIntervals={15} timeCaption="Hora" dateFormat="dd/MM/yyyy HH:mm" locale={ptBr}
            minDate={moment().toDate()} customInput={<input className="form-control"/>} />
        </div>
        <div className="form-group col-6">
          <div className="form-group">
            <label htmlFor="eventType">Tipo do evento</label>
            <select className="form-control" name='eventType' 
              onChange={e => {e.preventDefault(); setSelectedType(e.target.value);}}>
            {
              eventTypes.map((item, i) => (
                <option key={i} value={item.Id}>
                  {item.Type}
                </option>
              ))
              }
            </select>
          </div>
        </div>
        <div className='col-12'>
          <button type="submit" onClick={handleSubmit} className="btn btn-primary" disabled={isLoading}>
            {
              isLoading ?
              <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> :
              'Enviar'
            }
          </button>
        </div>
      </form>
    </PageContent>
  );
}

const mapDispatchToProps = (dispatch) => ({
  startAddEvent: event => dispatch(startAddEvent(event)),
  updateEventsStatus: status => dispatch(updateEventsStatus(status))
});

const mapStateToProps = (state) => ({
  events: state.events,
  token: state.auth.token
});

export default connect(mapStateToProps, mapDispatchToProps)(AddEventForm);
