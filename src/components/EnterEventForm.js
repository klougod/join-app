import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import Swal from 'sweetalert2';
import { history } from '../routers/MainRouter';
import { home } from '../routers/paths';
import { startParticipateEvent, updateEventsStatus } from '../actions/events';
import PageContent from './base/PageContent';


const ParticipateEventForm = ({ events, participateEvent }) => {

  const [eventId, setEventId] = useState(0);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if(events.isEnded){
      setIsLoading(false);
      updateEventsStatus(false);
      if(!events.error){
        Swal.fire('Tudo pronto', 'obrigado', 'success').then(() =>{
          history.push(home);
        });
      }
      else {
        Swal.fire('Erro ao participar de evento', 'tente novamente', 'warning');
      }
    }
  // eslint-disable-next-line
  }, [events])

  const handleSubmit = e => {
    e.preventDefault();
    setIsLoading(true);
    if(eventId){
      participateEvent(eventId);
    }
    else {
      Swal.fire('Erro ao adicionar evento', 'tente novamente', 'warning');
      setIsLoading(false);
    }
  }

  return (
    <PageContent>
      <form className='row justify-content-center'>
      <div className="form-group col-6">
          <div className="form-group">
            <label htmlFor="events">Escolha um evnto</label>
            <select className="form-control" name='events' 
              onChange={e => {e.preventDefault(); setEventId(e.target.value);}}>
            {
              Object.keys(events.events).map(key => {
                const item = events.events[key];
                return (
                  <option key={key} value={item.eventId}>
                  {item.name}
                </option>
              )})
              }
            </select>
          </div>
        </div>
        <div className='col-12'>
          <button type="submit" onClick={handleSubmit} className="btn btn-primary" disabled={isLoading}>
            {
              isLoading ?
              <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> :
              'Participar'
            }
          </button>
        </div>
      </form>
    </PageContent>
  );
}

const mapDispatchToProps = (dispatch) => ({
  participateEvent: id => dispatch(startParticipateEvent(id)),
  updateEventsStatus: status => dispatch(updateEventsStatus(status))
});

const mapStateToProps = (state) => ({
  events: state.events,
  token: state.auth.token
});

export default connect(mapStateToProps, mapDispatchToProps)(ParticipateEventForm);
