import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import BasePage from './BasePage';
import Event from '../models/Event';
import WillHappenEventBox from '../components/WillHappenEventBox';
import { getEventsToHappen } from '../webservices/event';


const EditEventPage = ({ token }) => {
  const [eventsToHappen, setEventsToHappen]= useState([]);

  useEffect(() => {
    const events = [];
    getEventsToHappen(token).then(({ data }) => {
      data.map((event) => {
        return events.push(Event.parse(event))
      })
      setEventsToHappen(events);
    }).catch(err => {
      console.log(err);
    })
  //eslint-disable-next-line
  }, []);

  return (
    <BasePage headerClass="header__bg" title="Próximos eventos">
      {
        eventsToHappen.map((event, i) => <WillHappenEventBox event={event} key={i} />)
      }
    </BasePage>
  )
};

const mapStateToProps = (state) => ({
  token: state.auth.token
});

export default connect(mapStateToProps)(EditEventPage);