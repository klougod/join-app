import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Card } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { startDeleteEvent, updateEventsStatus, startUpdateEvent } from '../actions/events';
import { history } from '../routers/MainRouter';
import { home } from '../routers/paths';


const EditEventBox = ({ event, deleteEvent, updateEvent, error, isEnded, updateEventsStatus }) => {
  const [isUpdateLoading, setIsUpdateLoading] = useState(false);
  const [isDeleteLoading, setIsDeleteLoading] = useState(false);
  const [name, setName] = useState(event ? event.name : '');
  const [description, setDescription] = useState(event ? event.description : '');

  useEffect(() => {
    if(isEnded){
      setIsDeleteLoading(false);
      setIsUpdateLoading(false);
      updateEventsStatus(false);
      if(!error){
        Swal.fire('Tudo pronto', 'obrigado', 'success').then(() =>{
          history.push(home);
        });
      }
      else {
        Swal.fire('Erro ao modificar evento', 'tente novamente', 'warning');
      }
    }
  }, [error, isEnded, updateEventsStatus, isDeleteLoading, isUpdateLoading])

  const handleDeleteEvent = e => {
    e.preventDefault();
    setIsDeleteLoading(true);
    if(event){
      deleteEvent(event.eventId);
    }
    else {
      Swal.fire('Erro ao deletar evento', 'tente novamente', 'warning');
      setIsDeleteLoading(false);
    }
  }

  const handleUpdateEvent = e => {
    e.preventDefault();
    setIsUpdateLoading(true);
    if(event){
      const eventUpdated = {
        ...event,
        name: name,
        description: description
      }
      updateEvent(eventUpdated);
    }
    else {
      Swal.fire('Erro ao adicionar evento', 'tente novamente', 'warning');
      setIsUpdateLoading(false);
    }
  }

  return (
    <div className="container h-100">
      <div className="row h-100 justify-content-center align-items-center">
        <div className="col-10">
          <Card>
            <Card.Body>
              <Card.Title>
                <input type="text" className="form-control" placeholder="Enter event name" name="name"
                  value={name} onChange={e => setName(e.target.value)} />
              </Card.Title>
              <Card.Subtitle>
                <input type="text" className="form-control" placeholder="Enter event description" name="description"
                  value={description} onChange={e => setDescription(e.target.value)} />
              </Card.Subtitle>
              <br />
              <div className='row'>
                <div className='col-6 text-center'>
                  <button type="button" onClick={handleUpdateEvent} className="btn btn-primary" disabled={isUpdateLoading}>
                    {
                      isUpdateLoading ?
                      <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> :
                      'Atualizar'
                    }
                  </button>
                </div>
                <div className='col-6 text-center'>
                  <button type="button" onClick={handleDeleteEvent} className="btn btn-danger" disabled={isDeleteLoading}>
                    {
                      isDeleteLoading ?
                      <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> :
                      'Deletar'
                    }
                  </button>
                </div>
              </div>
            </Card.Body>
          </Card>
        </div>
      </div>
    </div>
  );
}

const mapDispatchToProps = (dispatch) => ({
  deleteEvent: id => dispatch(startDeleteEvent(id)),
  updateEventsStatus: status => dispatch(updateEventsStatus(status)),
  updateEvent: eventData => dispatch(startUpdateEvent(eventData))
});

const mapStateToProps = (state) => ({
  token: state.auth.token,
  isEnded: state.events.isEnded,
  error: state.events.error
});

export default connect(mapStateToProps, mapDispatchToProps)(EditEventBox);
