import React from 'react';
import { connect } from 'react-redux';
import {Helmet} from 'react-helmet-async';
import Sidebar from '../components/Sidebar';
import { companyName } from '../constants';


export const BasePage = ({ title, description, children, isAuthenticated, noMenu }) => {

  const desc = description ? description : "Encontre eventos de esporte próximo a você!";

  return (
    <div id="outer-container">
      <Helmet>
        <title>{ `${title} | ${companyName}` }</title>
        <meta property="og:title" content={ `${title} | ${companyName}` } data-react-helmet="true" />
        <meta name="description" content={desc} data-react-helmet="true" />
        <meta property="og:description" content={desc} data-react-helmet="true" />
      </Helmet>
    {isAuthenticated && <Sidebar pageWrapId="page-wrap" outerContainerId="outer-container" noMenu={noMenu} />}
      <main id="page-wrap" style={{overflow: 'auto'}}>
        {children}
      </main>
    </div>
  );
}

const mapStateToProps = (state) => ({
  isAuthenticated: !!state.auth.token
});

export default connect(mapStateToProps)(BasePage);
