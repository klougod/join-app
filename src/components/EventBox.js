import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Card } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { startParticipateEvent, updateEventsStatus } from '../actions/events';
import { getParticipantsEvent } from '../webservices/event';
import { history } from '../routers/MainRouter';
import { home } from '../routers/paths';


const EventBox = ({ event, token, participateEvent, error, isEnded, updateEventsStatus }) => {
  const [participants, setParticipants] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if(isEnded){
      setIsLoading(false);
      updateEventsStatus(false);
      if(!error){
        Swal.fire('Tudo pronto', 'obrigado', 'success').then(() =>{
          history.push(home);
        });
      }
      else {
        Swal.fire('Erro ao participar de evento', 'tente novamente', 'warning');
      }
    }
  // eslint-disable-next-line
  }, [error, isEnded])

  useEffect(() => {
    if (event) {
      getParticipantsEvent(event.eventId, token).then(({ data }) => {
        setParticipants(data);
      }).catch(err => {
        console.log(err);
      })
    }
  }, [event, token])

  const handleSubmit = e => {
    e.preventDefault();
    setIsLoading(true);
    if(event){
      participateEvent(event.eventId);
    }
    else {
      Swal.fire('Erro ao adicionar evento', 'tente novamente', 'warning');
      setIsLoading(false);
    }
  }

  return (
    <div className="container h-100">
      <div className="row h-100 justify-content-center align-items-center">
        <div className="col-10">
          <Card>
            <Card.Body>
              <Card.Title>
                {event && event.name}
              </Card.Title>
              <Card.Subtitle>
                {event && event.description}
              </Card.Subtitle>
              <br />
              <select className="form-control" name='participants' placeholder='Participantes'>
              {
                participants.map((participant, i) => (
                  <option key={i}>{participant.Name}</option>
                ))
              }
              </select>
              <br />
              <div className='col-12 text-center'>
                <button type="submit" onClick={handleSubmit} className="btn btn-primary" disabled={isLoading}>
                  {
                    isLoading ?
                    <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> :
                    'Participar'
                  }
                </button>
              </div>
            </Card.Body>
          </Card>
        </div>
      </div>
    </div>
  );
}

const mapDispatchToProps = (dispatch) => ({
  participateEvent: id => dispatch(startParticipateEvent(id)),
  updateEventsStatus: status => dispatch(updateEventsStatus(status))
});

const mapStateToProps = (state) => ({
  token: state.auth.token,
  isEnded: state.events.isEnded,
  error: state.events.error
});

export default connect(mapStateToProps, mapDispatchToProps)(EventBox);
