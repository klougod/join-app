import React from 'react';
import BasePage from './BasePage';
import { Container, Row, Col } from 'react-bootstrap';


export const HomePage = () => {
  return (
    <BasePage isFooterFixed={true} headerClass="header__bg" title="Página não encontrada">
      <Container>
        <Row>
          <Col>Page Not Found</Col>
        </Row>
      </Container>
    </BasePage>
  );
}

export default HomePage;
