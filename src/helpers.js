export const setSessionItem = (key, item) => {
  try{
    return sessionStorage.setItem(key, JSON.stringify(item));
  }
  catch{
    return null;
  }
}

export const getSessionItem = (key) => {
  try{
    return JSON.parse(sessionStorage.getItem(key));
  }
  catch{
    return null;
  }
}

export const setLocalStorageItem = (key, item) => {
  try{
    return localStorage.setItem(key, JSON.stringify(item));
  }
  catch{
    return null;
  }
}

export const getLocalStorageItem = (key) => {
  try{
    return JSON.parse(localStorage.getItem(key));
  }
  catch{
    return null;
  }
}

export const removeLocalStorageItem = (key) => {
  try{
    return localStorage.removeItem(key);
  }
  catch{
    return null;
  }
}