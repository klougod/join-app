import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { history } from '../routers/MainRouter';
import { register, home } from '../routers/paths';
import { startLogin, updateAuthStatus } from '../actions/auth';
import PageContent from './base/PageContent';
import { emailRegex } from '../constants';
import Swal from 'sweetalert2';


const LoginForm = ({ startLogin, auth, updateAuthStatus }) => {

  const [email, setEmail] = useState('');
  const [emailError, setEmailError] = useState('');
  const [password, setPassword] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if(auth.isEnded){
      setIsLoading(false);
      updateAuthStatus(false);
      if(!auth.error){
        Swal.fire('Login efetuado', 'obrigado', 'success').then(() =>{
          history.push(home);
        });
      }
      else {
        Swal.fire('Erro ao fazer login', 'confira os dados e tente novamente', 'warning');
      }
    }
  }, [auth, isLoading, updateAuthStatus])

  const isInputValid = () => {
    if(!emailRegex.test(email)) {
      setEmailError('insira um email válido');
      return false;
    }
    else {
      setEmailError('');
    }
    if(password.length < 8){
      setPasswordError('o campo acima deve ter pelo menos 8 caracteres');
      return false;
    }
    else {
      setPasswordError('');
    }
    return true;
  }

  const handleSubmit = e => {
    e.preventDefault();
    setIsLoading(true);
    if(isInputValid()){
      const login = {
        email,
        password
      }
      startLogin(login);
    }
    else {
      setIsLoading(false);
    }
  }

  return (
    <PageContent>
      <form className='row'>
        <div className="form-group col-12">
          <label htmlFor="exampleInputEmail1">Endereço de email</label>
          <input type="email" className="form-control" placeholder="Enter email"
            value={email} onChange={e => setEmail(e.target.value)} />
          <small id="emailHelp" className="text-danger">
            {emailError}
          </small> 
        </div>
        <div className="form-group col-12">
          <label htmlFor="exampleInputPassword1">Senha</label>
          <input type="password" className="form-control" placeholder="Password"
            value={password} onChange={e => setPassword(e.target.value)} />
          <small id="passwordHelp" className="text-danger">
            {passwordError}
          </small> 
        </div>
        <div className='col-12'>
          <button type="submit" onClick={handleSubmit} className="btn btn-primary" disabled={isLoading}>
            {
              isLoading ?
              <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> :
              'Submit'
            }
          </button>
        </div>
        <div className='col-12'>
          <br /> <br />
          <h5>Ainda não tem uma conta?</h5>
          <button onClick={() => history.push(register)} className="btn btn-primary">Cadastre-se</button>
        </div>
      </form>
    </PageContent>
  );
}

const mapDispatchToProps = (dispatch) => ({
  startLogin: login => dispatch(startLogin(login)),
  updateAuthStatus: status => dispatch(updateAuthStatus(status))
});

const mapStateToProps = (state) => ({
  auth: state.auth
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
