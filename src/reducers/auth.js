// Auth reducer

const baseState = { isEnded: false, error: false, token: null };

export default (state = baseState, action) => {
  switch (action.type) {
    case 'SET_AUTH':
      return {
        ...state,
        ...action.login
      };
    case 'LOGOUT':
      return baseState;
    default:
      return state;
  }
};
