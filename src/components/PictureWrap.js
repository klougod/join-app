import React from 'react';


export const Refer = props => {

  const webp = src => (
    <source type="image/webp" srcSet={src} />
  );
  const jp2 = src => (
    <source type="image/jp2" srcSet={src} />
  );

  return (
    <picture className={props.className}>
      { props.webpSrc ? webp(props.webpSrc) : null}
      { props.jp2Src ? jp2(props.jp2Src) : null}
      <img src={props.src} width={props.width} height={props.height} alt={props.alt} />
    </picture>
  );
}


export default Refer;