FROM alpine as base

FROM base as builder

WORKDIR /
RUN apk update && apk --no-cache add nodejs-current npm
COPY ./package.json /package.json
RUN npm install --no-optional
COPY ./ /
RUN npm run build

FROM nginx:alpine

COPY default.conf /etc/nginx/conf.d/default.conf
COPY --from=builder /build /usr/share/nginx/html
