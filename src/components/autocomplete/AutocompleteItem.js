import React, { useRef } from 'react';


export const AutocompleteItem = props => {

  const itemRef = useRef(null);
  const address = props.address ? props.address : " ";
  const lat = props.lat ? props.lat : " ";
  const lng = props.lng ? props.lng : " ";

  const handleItemClick = e => {
    e.preventDefault();
    props.setInputValue(address);
    props.onSelectItem({ lat, lng, address });
    props.clearList();
  }

  const setActive = () => {
    itemRef.current.classList.add("autocomplete__item__active")
  }

  const removeActive = () => {
    itemRef.current.classList.remove("autocomplete__item__active")
  }

  return (
    <div
      ref={itemRef}
      onMouseOver={setActive}
      onMouseLeave={removeActive}
      onClick={handleItemClick}
      className="autocomplete__item"
    >
      {address}
    </div>
  );
}

export default AutocompleteItem;