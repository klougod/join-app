import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';
import { divIcon } from 'leaflet';
import { Marker, Popup } from 'react-leaflet';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Row, Col } from 'react-bootstrap';
import moment from 'moment';
import { history } from  '../../routers/MainRouter';


const PopupEvent = ({ name, description, date, position, icon, color, eventId }) => {
  const iconMarkup = renderToStaticMarkup(<FontAwesomeIcon size="2x" icon={icon} color={color ? color : 'black'} />);
  const customMarkerIcon = divIcon({ html: iconMarkup });
  return(
    <Marker position={position} icon={customMarkerIcon}>
      <Popup>
        <Row noGutters>
          <Col sm={12} className='bg-warning popup__col popup__col_title'>
            {name}
          </Col>
          <Col sm={12} className='popup__col popup__col_event'>
            {description}
          </Col>
          <Col sm={12} className='popup__col popup__col_event'>
            {date && moment(date).format('[Acontecerá ]DD/MM/YYYY HH:MM')}
          </Col>
          { eventId &&
            <Col sm={12} className='popup__col popup__col_button'>
              <button className='btn btn-outline-primary btn-sm' onClick={() => history.push(`/Event/${eventId}`)}>
                Ver
              </button>
            </Col>
          }
        </Row>
      </Popup>
    </Marker>
  )
}

export default PopupEvent;