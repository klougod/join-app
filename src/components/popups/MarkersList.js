import React, { Fragment } from 'react';
import PopupEvent from './PopupEvent';
import PopupEventList from './PopupEventList';

const MarkersList = ({ markers }) => {
  const items = markers.map(({ key, type, ...props }) => {
  return(
    type === 'single' ? <PopupEvent key={key} {...props} /> : <PopupEventList key={key} {...props} />
  )})
  return <Fragment>{items}</Fragment>
}

export default MarkersList;