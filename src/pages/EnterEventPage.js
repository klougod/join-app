import React from 'react';
import BasePage from './BasePage';
import EnterEventForm from '../components/EnterEventForm';


const EnterEventPage = props => {

  return (
    <BasePage headerClass="header__bg" title="Participar Evento">
      <EnterEventForm />
    </BasePage>
  );
}

export default EnterEventPage;
