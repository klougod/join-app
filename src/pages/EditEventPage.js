import React from 'react';
import { connect } from 'react-redux';
import BasePage from './BasePage';
import EventBox from '../components/EventBox';


const EditEventPage = ({ event }) => (
  <BasePage headerClass="header__bg" title="Editar Evento">
    <EventBox event={event} />
  </BasePage>
);

const mapStateToProps = (state, props) => ({
  event: state.events.events[props.match.params.id]
});

export default connect(mapStateToProps)(EditEventPage);