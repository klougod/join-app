import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import ScrollToTop from './ScrollToTop';
import HomePage from '../pages/HomePage';
import LoginPage from '../pages/LoginPage';
import RegisterPage from '../pages/RegisterPage';
import NotFoundPage from '../pages/NotFoundPage';
import AddEventPage from '../pages/AddEventPage';
import EnterEventPage from '../pages/EnterEventPage';
import EditEventPage from '../pages/EditEventPage';
import UserEventsPage from '../pages/UserEventsPage';
import EventsToHappenPage from '../pages/EventsToHappenPage';
import PrivateRoute from './PrivateRoute';
import { home, login, register, addEvent, enterEvent, editEvent, userEvents, eventsToHappen } from './paths';


export const history = createBrowserHistory();

const MainRouter = () => (
  <Router history={ history }>
    <ScrollToTop>
      <Switch>
        <Route path={ register } component={ RegisterPage } exact />
        <Route path={ login } component={ LoginPage } exact />
        <PrivateRoute path={ home } component={ HomePage } exact />
        <PrivateRoute path={ addEvent } component={ AddEventPage } exact />
        <PrivateRoute path={ enterEvent } component={ EnterEventPage } exact />
        <PrivateRoute path={ editEvent } component={ EditEventPage } exact />
        <PrivateRoute path={ userEvents } component={ UserEventsPage } exact />
        <PrivateRoute path={ eventsToHappen } component={ EventsToHappenPage } exact />
        <Route component={ NotFoundPage } />
      </Switch>
    </ScrollToTop>
  </Router>
);

export default MainRouter;
