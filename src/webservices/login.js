import { post } from './base';


export const postRegister = data => {
  return post({url: '/Users/Register', data});
}

export const postLogin = data => {
  return post({url: '/Users/Login', data});
}