import React from 'react';
import BasePage from './BasePage';
import LoginForm from '../components//LoginForm';


const LoginPage = props => {

  return (
    <BasePage noMenu headerClass="header__bg" title="Login">
      <LoginForm />
    </BasePage>
  );
}

export default LoginPage;
