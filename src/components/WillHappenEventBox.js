import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Card } from 'react-bootstrap';
import moment from 'moment';
import { startParticipateEvent, updateEventsStatus } from '../actions/events';
import { getParticipantsEvent } from '../webservices/event';


const WillHappenEventBox = ({ event, token, participateEvent, error, isEnded, updateEventsStatus }) => {
  const [participants, setParticipants] = useState([]);

  useEffect(() => {
    if (event) {
      getParticipantsEvent(event.eventId, token).then(({ data }) => {
        setParticipants(data);
      }).catch(err => {
        console.log(err);
      })
    }
  }, [event, token])

  return (
    <div className="container h-100">
      <div className="row h-100 justify-content-center align-items-center">
        <div className="col-10">
          <Card>
            <Card.Body>
              <Card.Title>
                {event && event.name}
              </Card.Title>
              <Card.Subtitle>
                {event && event.description}
              </Card.Subtitle>
              <Card.Subtitle>
                <br />
                {event && moment(event.date).format('[Acontecerá ]DD/MM/YYYY HH:MM')}
              </Card.Subtitle>
              <br />
              <select className="form-control" name='participants' placeholder='Participantes'>
              {
                participants.map((participant, i) => (
                  <option key={i}>{participant.Name}</option>
                ))
              }
              </select>
            </Card.Body>
          </Card>
        </div>
      </div>
    </div>
  );
}

const mapDispatchToProps = (dispatch) => ({
  participateEvent: id => dispatch(startParticipateEvent(id)),
  updateEventsStatus: status => dispatch(updateEventsStatus(status))
});

const mapStateToProps = (state) => ({
  token: state.auth.token,
  isEnded: state.events.isEnded,
  error: state.events.error
});

export default connect(mapStateToProps, mapDispatchToProps)(WillHappenEventBox);
